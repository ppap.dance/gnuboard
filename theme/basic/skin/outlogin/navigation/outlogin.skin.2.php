<?php
if (!defined("_GNUBOARD_")) exit; // 개별 페이지 접근 불가

// add_stylesheet('css 구문', 출력순서); 숫자가 작을 수록 먼저 출력됨
add_stylesheet('<link rel="stylesheet" media="screen and (max-width:1199px)" href="'.$outlogin_skin_url.'/style.css">', 0);
?>

<aside id="ol_after" class="ol">
  <h2>나의 회원정보</h2>
  <div id="ol_after_hd">
    <span class="profile_img">
        <?php echo get_member_profile_img($member['mb_id']); ?>
      <a href="<?php echo G5_BBS_URL ?>/member_confirm.php?url=register_form.php" id="ol_after_info" title="정보수정"><i class="fa fa-cog fa-fw"></i></a>
    </span>
    <strong><?php echo $nick ?>님</strong>
    <div id="ol_after_btn">
        <?php if ($is_admin == 'super' || $is_auth) { ?><a href="<?php echo G5_ADMIN_URL ?>" class="btn_admin"><i class="fa fa-cog fa-spin fa-fw"></i></a><?php } ?>
      <a href="<?php echo G5_BBS_URL ?>/logout.php" id="ol_after_logout">로그아웃</a>
    </div>
  </div>
</aside>