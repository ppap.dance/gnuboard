<?php
if (!defined("_GNUBOARD_")) exit; // 개별 페이지 접근 불가

// add_stylesheet('css 구문', 출력순서); 숫자가 작을 수록 먼저 출력됨
add_stylesheet('<link rel="stylesheet" media="screen and (max-width:1199px)" href="'.$outlogin_skin_url.'/style.css">', 0);
?>

<div id="ol_before" class="ol">
  <h2>회원로그인</h2>
  <a href="/bbs/login.php" class="btn_b01">로그인</a>
  <a href="/bbs/register.php" class="btn_b02">회원가입</a>
</div>