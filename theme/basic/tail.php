<?php
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가
?>
      </div>
      <div class="col-xs-3 hidden-xs visible-lg">
        <?php echo outlogin('theme/aside'); ?>
      </div>
    </div>
  </div>
</div>
<!-- } 콘텐츠 끝 -->

<!-- 하단 시작 { -->
<div class="footer">
  <div class="footer__container container">
    <h2 class="footer__logo">
      <img src="<?php echo G5_IMG_URL ?>/logo.png" alt="" title="">
    </h2>
    <p class="footer__address">
      <span><?php echo $g5['title'] ?></span>
      <i class="separator">,</i>
      <span>고객센터 0000-0000</span>
    </p>
    <p class="footer__address">
      <span>사업자주소</span>
      <i class="separator">,</i>
      <span>대표 ???</span>
      <i class="separator">,</i>
      <span>사업자등록번호 000-00-00000</span>
    </p>
    <p class="footer__copyright">
      Copyright &copy; <?php echo $g5['title'] ?> All Rights Reserved.
    </p>
  </div>
</div>

<div class="sticky">
  <div class="sticky__container container">
    <div class="sticky__header">
      <h2 class="sticky__title">
        전문 상담원이 안내해드립니다!
      </h2>
      <p class="sticky__caption">
        <a href="tel:1668-1089">
          0000-0000
        </a>
      </p>
    </div>
    <div class="sticky__content">
      <a class="sticky__button button mobile-only" href="tel:1668-1089" rel="nofollow">
        무료전화상담
      </a>
      <form class="sticky__form" action="/" method="post">
        <input class="sticky__text-field text-field desktop-only" type="text" name="name" placeholder="이름" readonly="readonly"/>
        <input class="sticky__text-field text-field desktop-only" type="phone" name="phone_number" placeholder="휴대폰번호" readonly="readonly"/>
        <input class="sticky__text-field text-field desktop-only" type="text" name="comment" placeholder="상담가능시간" readonly="readonly"/>
        <button class="sticky__form__button sticky__button button" type="submit">
          간편상담신청
        </button>
      </form>
      <a href="http://pf.kakao.com/_ZTXFj/chat" target="_blank" class="sticky__button button button--kakaotalk">
        카톡상담하기
      </a>
    </div>
  </div>
</div>
<script>
  jQuery(function ($) {

    $('.sticky__form').on('submit', function (event) {
      event.preventDefault();
      event.stopImmediatePropagation();

      $('.popup').addClass('popup--visible');
      return false;
    });

    $('.sticky__text-field').on('click touchend', function (event) {
      event.preventDefault();
      event.stopImmediatePropagation();

      $('.popup').addClass('popup--visible');
      return false;
    });
  });
</script>

<?php
$qaconfig = get_qa_config();
?>
<div class="popup">
  <div class="popup__content">
    <form class="popup__form" method="post" enctype="multipart/form-data" action="<?php echo https_url(G5_BBS_DIR).'/qawrite_update.php' ?>">
      <input type="hidden" name="w" value=""/>
      <input type="hidden" name="qa_html" value="0"/>
      <input type="hidden" name="qa_subject" value="간편 상담신청 합니다."/>
      <div class="popup__header">
        <h2 class="popup__title">
          간편상담신청
        </h2>
        <a class="popup__header__button" href="javascript:;">
          <i class="fa fa-times"></i>
        </a>
        <p class="popup__caption">
          상담신청을 하시면 전문상담원이 전화를 드립니다!
        </p>
      </div>
      <div class="popup__container">
        <div class="popup__row row">
          <div class="col-xs-4">
            분류
          </div>
          <div class="col-xs-8">
            <select name="qa_category" class="popup__select">
              <option value="">분류를 선택하세요</option>
              <?php foreach (explode('|', $qaconfig['qa_category']) as $qa_category) { ?>
                <option value="<?php echo $qa_category ?>"><?php echo $qa_category ?></option>
              <?php } ?>
            </select>
          </div>
        </div>
        <div class="popup__row row">
          <div class="col-xs-4">
            휴대폰번호
          </div>
          <div class="col-xs-8">
            <input type="text" name="qa_hp" class="popup__text-field"/>
          </div>
        </div>
        <div class="popup__row row">
          <div class="col-xs-4">
            상담가능시간
          </div>
          <div class="col-xs-8">
            <input type="text" name="qa_content" class="popup__text-field"/>
          </div>
        </div>
      </div>
      <div class="popup__footer">
        <button type="submit" class="popup__footer__button">
          확인
        </button>
      </div>
    </form>
  </div>
</div>

<script>
  jQuery(function ($) {
    $('.popup__header__button')
      .click(function () {
        $('.popup').removeClass('popup--visible');
      });
  });
</script>

<?php
if ($config['cf_analytics']) {
    echo $config['cf_analytics'];
}
?>

<!-- } 하단 끝 -->

<?php
include_once(G5_THEME_PATH."/tail.sub.php");
?>