<?php
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가

include_once(G5_THEME_PATH.'/head.sub.php');
include_once(G5_LIB_PATH.'/outlogin.lib.php');
?>

<script >
  jQuery(function ($) {
    $('.header__hamburger.hamburger')
      .click(function () {
        $(this).toggleClass('hamburger--crossed');

        var isCrossed = $(this).hasClass('hamburger--crossed');
        if (isCrossed) {
          $('.navigation').show();
        } else {
          $('.navigation').hide();
        }
      })

    $('.navigation__exit')
      .click(function () {
        $('.navigation').toggle();
        $('.header__hamburger.hamburger').toggleClass('hamburger--crossed');
      })
  });
</script>
<div class="header">
  <div class="header__header">
      <h2 class="header__title">
        빠른 <strong class="header__strong">인터넷</strong>상담
      </h2>
      <a class="header__link" href="tel:1668-1089" rel="nofollow">
        0000-0000
      </a>
  </div>
  <div class="header__container container">
    <h1 class="header__logo">
      <a href="/" class="header__link">
        <img class="header__image" src="<?php echo G5_IMG_URL ?>/logo.png" alt="<?php echo $g5['title'] ?>" title="<?php echo $g5['title'] ?>">
      </a>
    </h1>
    <div class="header__hamburger hamburger">
      <span class="hamburger__stack"></span>
      <span class="hamburger__stack"></span>
      <span class="hamburger__stack"></span>
    </div>
  </div>
  <div class="header__footer">
    <div class="header__footer__container container">
      <div class="navigation">
        <a href="#" class="navigation__exit">
          <i class="fa fa-times" aria-hidden="true"></i>
        </a>
        <div class="hidden-lg">
          <?php echo outlogin('theme/navigation'); ?>
        </div>
        <ul class="navigation__list">
          <?php
          $sql = " select *
                   from {$g5['menu_table']}
                   where me_use = '1'
                   and length(me_code) = '2'
                   order by me_order, me_id ";

          $result = sql_query($sql);
          while ($row = sql_fetch_array($result)) {
          ?>
            <li class="navigation__list__item">
              <a href="<?php echo $row['me_link']; ?>" class="navigation__list__item__link" target="_<?php echo $row['me_target']; ?>">
                <?php echo $row['me_name']; ?>
              </a>
              <?php
              $sql2 = " select *
                        from {$g5['menu_table']}
                        where me_use = '1'
                        and length(me_code) = '4'
                        and substring(me_code, 1, 2) = '{$row['me_code']}'
                        order by me_order, me_id ";
              $result2 = sql_query($sql2);
              if ($row2 = sql_fetch_array($result2)) {
              ?>
                <ul class="navigation__sublist">
                <?php
                do {
                ?>
                  <li class="navigation__sublist__item">
                    <a href="<?php echo $row2['me_link']; ?>" class="navigation__sublist__item__link" target="_<?php echo $row2['me_target']; ?>">
                        <?php echo $row2['me_name']; ?>
                    </a>
                  </li>
                <?php
                } while ($row2 = sql_fetch_array($result2));
                ?>
                </ul>
                <?php
              }
              ?>
            </li>
          <?php
          }
          ?>
        </ul>
      </div>
    </div>
  </div>
</div>

<?php
if(defined('_INDEX_')) { // index에서만 실행
  include G5_BBS_PATH.'/newwin.inc.php'; // 팝업레이어
}
?>

<!-- 콘텐츠 시작 { -->
<div class="content">
  <div class="content__container container">
    <div class="row">
      <div class="col-xs-12 col-lg-9">

